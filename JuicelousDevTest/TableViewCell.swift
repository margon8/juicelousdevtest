//
//  TableItemViewCell.swift
//  JuicelousDevTest
//
//  Created by Marc Gonzalez Ramos on 05/01/2018.
//  Copyright © 2018 Marc Gonzalez Ramos. All rights reserved.
//

import UIKit

class TableItemViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!

}

class TableButtonViewCell: UITableViewCell {
    
    var parent: ViewController?
    
    @IBAction func addTapped(_ sender: Any) {
        parent?.addNewItem()
    }
    
}
