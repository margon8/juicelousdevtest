//
//  ViewController.swift
//  JuicelousDevTest
//
//  Created by Marc Gonzalez Ramos on 05/01/2018.
//  Copyright © 2018 Marc Gonzalez Ramos. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var items: [String] = ["First","Second","Third"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count+1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < items.count {
            
            let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! TableItemViewCell
            itemCell.nameLabel.text = items[indexPath.row]
            return itemCell
            
        } else {
            
            let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! TableButtonViewCell
            buttonCell.parent = self
            return buttonCell
            
        }

    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.row == items.count {
            return false
        } else {
            return true
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            items.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
    }

}

extension ViewController {
    
    public func addNewItem() {
        
        items.append("New")

        self.tableView.reloadData()
        
    }
}

