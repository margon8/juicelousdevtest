## Juicelous iOS Developer Test

Thank you for your interest in joining the Juicelous Engineering team. Here you'll find a XCode 8 project with a Swift 4 iOS app and a watchOS extension. You are required to submit a pull request with the first or both of the fulfilled requirements below:

---

**1. Make the tables synchronous**

In both iOS app and watchOS extension you'll find a table that you can add and delete rows. Your main task is to sychronize them so the changes you made on one are visible on the other and viceversa.

Here are some tips:
1. Consider using WatchConnectivity framework.
2. The simulator provides a paired and available watch, but that might not be the case.
3. Text labels are used for simplicity but do not assume its lightness (e.g: a row containing a video file)

---

**2. Make the tables persistent (Optional)**

Next, you�ll add persistency to the tables. Despite there are several techniques out there, we internally use Parse framework, which it is already included and set up in the project. You are encouraged to use Parse if you are familiar with it or to use another method that you're more confortable with.

Again, some tips are:
1. Keep it as simple as possible.
2. WkInterfaceControllers are more volatile than UIViewControllers.
3. One or both devices might not have internet connectivity at certain times.

Best of luck and you can submit all questions reaching hello@juicelous.com