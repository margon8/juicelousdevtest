//
//  InterfaceController.swift
//  JuicelousDevTest WatchKit Extension
//
//  Created by Marc Gonzalez Ramos on 05/01/2018.
//  Copyright © 2018 Marc Gonzalez Ramos. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    var items: [String] = ["First","Second","Third"]

    @IBOutlet var table: WKInterfaceTable!
    
    func loadTable() {
        
        table.setNumberOfRows(items.count, withRowType: "ItemCell")
        
        for (i,name) in items.enumerated() {
            
            let rc = table.rowController(at: i) as! TableItemObjectCell
            rc.row = i
            rc.parent = self
            rc.nameLabel.setText(name)
            
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        loadTable()
        
    }
    
    func delete(row: Int) {
        
        items.remove(at: row)
        
        loadTable()
    }

    @IBAction func add() {
        items.append("New")
        
        loadTable()
    }
}

open class TableItemObjectCell: NSObject {
    
    var row: Int?
    var parent: InterfaceController?
    
    @IBOutlet var nameLabel: WKInterfaceLabel!
    
    @IBAction func deleteRow() {
        parent?.delete(row: row!)
    }
}
